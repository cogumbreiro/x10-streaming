public abstract class AbstractClockedWriter[T] extends Clocked implements ClockedWriter[T] {
    public def this(clock:Clock) {
        super(clock);
    }

    public operator this()=(value:T) {set(value);}
    
    public abstract def set(value:T):void;
}

