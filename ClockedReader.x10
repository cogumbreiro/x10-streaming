import x10.util.ArrayList;

public interface ClockedReader[T] extends Reader[T] {
    property def clock(): Clock;
}
