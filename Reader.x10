import x10.util.ArrayList;

public interface Reader[T] extends () => T {
    def get() throws EOSException:T;
    operator this() throws EOSException:T;
}
