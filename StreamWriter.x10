import x10.util.concurrent.AtomicBoolean;

public class StreamWriter[T] extends AbstractClockedWriter[T] {
    private val cursor:DoubleBuffer[Buffer[T]];
    private var buffer:Rail[T];
    private var eos:Boolean = false;
    private var idx:Int = 0;
    private val offset: Int;
    private val size: Int;
    
    public def this(cursor: DoubleBuffer[Buffer[T]], offset: Int, size: Int) {
        super(cursor.clock);
        this.cursor = cursor;
        this.buffer = cursor.get().data;
        this.offset = offset;
        this.size = size;
        Utils.checkRange(offset, size, buffer.size);
    }

    public def set(value:T):void {
        if (eos) throw new EOSException();
        buffer(offset + idx) = value;
        idx += 1;
        if (idx == size) {
            cursor.advance();
            this.buffer = cursor.get().data;
            idx = 0;
        }
        assert idx < buffer.size; 
    }
    
    public def close():void {
        eos = true;
        assert idx <= buffer.size;
        cursor.get().close(idx);
        cursor.advance();
    }
}

