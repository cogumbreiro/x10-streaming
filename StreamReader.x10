public class StreamReader[T] extends AbstractClockedReader[T] {

    private val offset:Int;
    private var size:Int;
    private val cursor:DoubleBuffer[Buffer[T]];
    private val computeSize: (Int) => Int;
    private var buffer:Rail[T];
    private var idx:Int;
    private var eos:Boolean;
    
    private def this(cursor: DoubleBuffer[Buffer[T]],
                     offset: Int, size: Int,
                     computeSize: (Int) => Int,
                     idx: Int, eos: Boolean) {
        super(cursor.clock);
        this.offset = offset;
        this.size = size;
        this.cursor = cursor;
        this.computeSize = computeSize;
        this.buffer = cursor.get().data;
        this.idx = idx;
        this.eos = eos;
        Utils.checkRange(offset, size, buffer.size);
    }
    
    public def this(cursor: DoubleBuffer[Buffer[T]], offset: Int, size: Int,
                    computeSize: (Int) => Int) {
        this(cursor, offset, size, computeSize, size, false);
    }
    
    public def get() throws EOSException:T {
        if (eos) {
            throw new EOSException();
        }
        assert idx <= size;
        if (idx == size) { // it's empty
            fetch();
        }
        return buffer(offset + idx++);
    }
    
    private def fetch() {
        if (cursor.get().isClosed) {
            eos = true;
            throw new EOSException();
        }
        cursor.advance();
        if (cursor.get().isClosed) {
            size = computeSize(cursor.get().size);
        }
        idx = 0;
        if (idx == size) {
            eos = false;
            throw new EOSException();
        }
        buffer = cursor.get().data;
    }
    
    public def copy() {
        return new StreamReader[T](cursor.copy(), offset, size, computeSize, idx, eos);
    }
}

