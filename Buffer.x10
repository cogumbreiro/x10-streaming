public interface Buffer[T] {
    property def data(): Rail[T];
    property def size(): Int;
    def close(size: Int): void;
    property def isClosed(): Boolean;
}
