public class Deadlock2 {
    public static def main(Array[String](1)):void {
        val clock = Clock.make("c");
            finish {
                for (i in 1..3) async clocked(clock) {
                    finish {
                        System.sleep(1000);
                        try { Clock.advanceAll(); }
                        catch (ClockUseException) {clock.drop();}
                    }
                }
            }
    }
}




























//async clocked(clock) {Console.OUT.println(1);finish{finish{Clock.advanceAll();}}}

