public abstract class Clocked {
    public def this(clock:Clock) {
        this.clock = clock;
    }

    private val clock:Clock;

    public property clock() = clock;
}
