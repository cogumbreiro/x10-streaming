/**
 * Multiple writers, one reader.
 */
public struct Mux[T](writers:Rail[ClockedWriter[T]], reader:ClockedReader[T]) {
    public static def make[T](writerCount:Rail[Int], clock:Clock) {T haszero} =
        make[T](writerCount, clock, Zero.get[T]());
        
    public static def make[T](writerCount:Int, clock:Clock) {T haszero} =
        make[T](writerCount, clock, Zero.get[T]());

    public static def make[T](writerCount:Int, clock:Clock, zero:T): Mux[T] =
        make[T](new Rail[Int](writerCount, 1), clock, zero);

    public static def make[T](writerCount:Rail[Int], clock:Clock, zero:T): Mux[T] {self.writers.size == writerCount.size} {
        val totalSize = Utils.sumSizes(writerCount);
        val offsets = Utils.sizesToOffsets(writerCount);

        val elem1 = new BufferImpl[T](new Rail[T](totalSize, zero));
        val elem2 = new BufferImpl[T](new Rail[T](totalSize, zero));
        val writers = new Rail[ClockedWriter[T]](writerCount.size,
            (x:Int) => {
                val buffer = new DoubleBuffer[Buffer[T]](clock, elem1, elem2);
                return new StreamWriter[T](buffer, offsets(x), writerCount(x));
            });
        val buff = new DoubleBuffer[Buffer[T]](clock, elem2, elem1);
        val reader = new StreamReader[T](buff, 0, totalSize, (x:Int) => 0);
        return new Mux[T](writers, reader);
    }

    public static def main(args:Array[String](1)):void {
        finish async {
            val T = 3;
            val clock = Clock.make();
            val writeCount = new Rail[Int](T, (x:Int) => x + 1);
            val dem:Mux[Int] = make[Int](writeCount, clock);
            val N = 20;
            for (idx in dem.writers) {
                val writer = dem.writers(idx);
                async clocked(clock) {
                    for (p in 1..N) {
                        for (_ in 1..(idx(0) + 1)) {
                            writer() = (idx(0) + 1) * p;
                        }
                    }
                    writer.close();
                }
            }

            async clocked(clock) {
                val reader = dem.reader;
                try {
                    while (true) {
                        val result = reader();
                        Console.OUT.println(result);
                    }
                } catch (EOSException) {
                }
            }
        }
    }
}
