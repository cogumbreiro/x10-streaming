import x10.util.Pair;
/**
 * One writer, multiple readers.
 */
public struct Broadcast[T](writer:ClockedWriter[T], readers: Rail[ClockedReader[T]]) {
    public static def make[T](cacheSize:Int,
                                       readerCount:Int) {T haszero} =
        make[T](readerCount, cacheSize, Zero.get[T]());
        
    public static def make[T](cacheSize:Int, readerCount:Int, zero:T):Broadcast[T] =
        make[T](cacheSize, readerCount, Clock.make(), zero);
        
    public static def make[T](cacheSize:Int, readerCount:Int, clock:Clock) {T haszero} =
        make[T](cacheSize, readerCount, clock, Zero.get[T]());
        
    public static def make[T](cacheSize:Int, readerCount:Int, clock:Clock, zero:T):Broadcast[T] {
        if (readerCount < 1) {
            throw new IllegalArgumentException();
        }
        val pair = Stream.makePair[T](cacheSize, clock, zero);
        
        val readers = new Rail[ClockedReader[T]](readerCount,
            (x:Int) => x == 0 ? pair.second : pair.second.copy());
        return new Broadcast[T](pair.first, readers);
    }
    

    public static def main(args:Array[String](1)):void {
        finish async {
            val T = 3;
            val clock1 = Clock.make("clock1");
            val clock2 = Clock.make("clock2");
            val db1 = make[Int](T, 2, clock1);
            val db2 = make[Int](T, 2, clock2);
            val N = 100000;
            //val N = 10;
            async clocked(clock1, clock2) {
                val writer1 = db1.writer;
                val writer2 = db2.writer;
                for (p in 1..N) {
                    writer1() = p;
                    writer2() = p;
                    //Console.OUT.println("! " + (p + p));
                }
                writer1.close();
                writer2.close();
            }
            for (p in 0..(T-1)) {
                val reader1 = db1.readers(p);
                val reader2 = db2.readers(p);
                
                async clocked(clock1, clock2) {
                    try {
                        while (true) {
                            val result:Int = reader1() + reader2();
                            //Console.OUT.println(result);
                        }
                    } catch (EOSException) {
                    }
                }
            }
        }
    }
}
