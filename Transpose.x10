/*
 * int -> int splitjoin Transpose(int m, int n) {
 *    split roundrobin(1);
 *    for (int i = 0; i < n; i++) {
 *        add Identity<int>;
 *    }
 *    join roundrobin(m);
 * }
 */
public struct Transpose(m:Int, n:Int) {
    public static class Identity[T] extends Filter[T,T] {
        public def this() {
            super(1, 1);
        }
        
        public def work() {
            push(pop());
        }
    }
    
    public static class Printer[T] extends Filter[T,_] {
        public def this() {
            super(1, 0);
        }
        
        public def work() {
            Console.OUT.println(pop());
        }
    }
    
    public static class In(n:Int) extends Filter[_,Int] {
        public def this(n:Int) {
            super(0, n);
            property(n);
        }
        public def work() {
            for (x in 1..n) {
                push(x);
            }
        }
    }
    
    public def run() {
        val clock1 = Clock.make();
        val demux = Demux.make[Int](n, clock1);
        val clock2 = Clock.make();
        val mux = Mux.make[Int](new Rail[Int](n, (x:Int) => m), clock2);
        
        async clocked(clock1) {
            val f = new In(n * m);
            f.setOutput(demux.writer);
            f.work();
            f.work();
            f.eos();
        }

        // now build the identity filters
        for (idx in demux.readers) {
            async clocked(clock1, clock2) {
                val reader = demux.readers(idx);
                val writer = mux.writers(idx);
                try {
                    while(true) {
                        writer() = reader();
                    }
                } catch (EOSException) {
                    writer.close(); // propagate
                }
            }
        }
        
        async clocked(clock2) {
            val f = new Printer[Int]();
            f.setInput(mux.reader);
            try {
                while(true) {
                    f.work();
                }
            } catch (EOSException) {
            }
        }
        
    }
    public static def main(args:Array[String](1)):void {
        new Transpose(2,2).run();
    }
}
