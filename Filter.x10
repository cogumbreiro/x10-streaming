import x10.util.ArrayList;

public abstract class Filter[I,O](pop:Int, push:Int) {
    private val buffer = new ArrayList[I]();
    private var _input: ClockedReader[I];

    private var _output: ClockedWriter[O];

    public property def input() {return _input;}
    
    public def setInput(inp: ClockedReader[I]) {
        _input = inp;
    }

    public property def output() {return _output;}
    public def setOutput(out: ClockedWriter[O]) {_output = out;}
    
    public def pop(): I {
        return buffer.size() == 0 ? _input() : buffer.removeAt(0);
    }
    
    public def peek(idx:Int): I {
        for (i in 1 .. (idx - buffer.size())) {
            buffer.add(_input());
        }
        return buffer.get(idx);
    }
    
    public def push(value: O) {
        _output() = value;
    }
    
    public def eos(): void {
        _output.close();
    }
        
    public abstract def work(): void;
}

