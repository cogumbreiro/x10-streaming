public struct Utils {
    public static def checkRange(offset: Int, size: Int, totalSize: Int) {
        if (0 > offset || offset > totalSize) {
            throw new IllegalArgumentException("offset= " + offset + ", expected 0 <= offset <= " + totalSize);
        }
        if (size < 1 || size > totalSize) {
            throw new IllegalArgumentException("size= " + size + ", expected 1 <= size <= " + totalSize);
        }
        if (size + offset > totalSize) {
            throw new IllegalArgumentException("size + offset = " + (size + offset) + ", expected size + offset <= " + totalSize);
        }
    }
    
    public static def sizesToOffsets(sizes:Rail[Int]) {
        val offsets = new Rail[Int](sizes.size, 0);
        var offset:Int = 0;
        for (idx in sizes) {
            if (sizes(idx) <= 0) {
                throw new IllegalArgumentException("The size at index " +
                    idx + " must be positive: " + sizes(idx));
            }
            offsets(idx) = offset;
            offset += sizes(idx);
        }
        return offsets;
    }
    
    public static def sumSizes(sizes:Rail[Int]) {
        val totalSize = sizes.reduce((x:Int, y:Int) => x + y, 0);
        if (totalSize <= 0) {
            throw new IllegalArgumentException("The number of elements o be " +
                "written must be greater than 0" + totalSize);
        }
        return totalSize;
    }
}
