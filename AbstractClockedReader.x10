public abstract class AbstractClockedReader[T] extends Clocked implements ClockedReader[T] {
    public def this(clock:Clock) {
        super(clock);
    }
    
    public operator this():T {
        return get();
    }
    public abstract def get() throws EOSException:T;
}
