import x10.util.Pair;

/**
 * Defines a the type of a point-to-point communication.
 */
public struct Stream[T] (writer: ClockedWriter[T], reader: ClockedReader[T]) {
    public static def make[T](cacheSize:Int) {T haszero} =
        make[T](cacheSize, Clock.make());

    public static def make[T](cacheSize:Int, clock:Clock) {T haszero} =
        make[T] (cacheSize, clock, Zero.get[T]());

    protected static def makePair[T](cacheSize:Int, clock:Clock, zero:T) {
        val elem1 = BufferImpl[T](new Rail[T](cacheSize, zero));
        val elem2 = BufferImpl[T](new Rail[T](cacheSize, zero));
                
        val wbuffer = new DoubleBuffer[Buffer[T]](clock, elem2, elem1);
        val writer = new StreamWriter[T](wbuffer, 0, cacheSize);
        
        val rbuffer = new DoubleBuffer[Buffer[T]](clock, elem1, elem2);
        val reader = new StreamReader[T](rbuffer, 0, cacheSize, (x:Int) => x);

        return new Pair[StreamWriter[T], StreamReader[T]](writer, reader);
    }
    
    public static def make[T](cacheSize:Int, clock:Clock, zero:T) {
        val p = makePair[T](cacheSize, clock, zero);
        return new Stream[T](p.first, p.second);
    }

    public static def main(args:Array[String](1)):void {
        finish async {
            val clock1 = Clock.make("clock1");
            val stream = make[Int](3, clock1);
            val N = 10;
            //val N = 10;
            async clocked(clock1) {
                val writer1 = stream.writer;
                for (p in 1..N) {
                    writer1() = p;
                }
                writer1.close();
            }
            
            
            async clocked(clock1) {
                val reader1 = stream.reader;
                try {
                    while (true) {
                        val result:Int = reader1();
                        Console.OUT.println(result);
                    }
                } catch (EOSException) {}
            }
        }
    }
}
