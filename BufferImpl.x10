import x10.util.concurrent.AtomicBoolean;

public struct BufferImpl[T] implements Buffer[T] {
    private val _isClosed: AtomicBoolean = new AtomicBoolean(false);
    private val _data: Rail[T];
    private val _size: Cell[Int];
    
    public def this(data:Rail[T]) {
        this._data = data;
        this._size = new Cell[Int](data.size);
    }
    
    public property def isClosed() {
        return _isClosed.get();
    }
    
    public property def data() {
        return _data;
    }
    
    public def close(count: Int) {
        _isClosed.set(true);
        _size() = count;
    }
    
    public property def size() {
        return _size();
    }
}

