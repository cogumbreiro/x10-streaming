import x10.util.Pair;
public struct Demux[T](writer: ClockedWriter[T], readers: Rail[ClockedReader[T]]) {
    public static def make[T](readerCount:Int, clock:Clock) {T haszero} =
        make(readerCount, clock, Zero.get[T]());

    public static def make[T](readerCount:Rail[Int], clock:Clock) {T haszero} =
        make(readerCount, clock, Zero.get[T]());

    public static def make[T](readerCount:Int, clock:Clock, zero:T) =
        make[T](new Rail[Int](readerCount, 1), clock, zero);
    
    public static def make[T](readerCount:Rail[Int], clock:Clock, zero:T):Demux[T] {
        val totalSize = Utils.sumSizes(readerCount);
        val offsets = Utils.sizesToOffsets(readerCount);

        val elem1 = BufferImpl[T](new Rail[T](totalSize, zero));
        val elem2 = BufferImpl[T](new Rail[T](totalSize, zero));
        
        val wbuff = new DoubleBuffer[Buffer[T]](clock, elem1, elem2);
        val writer = new StreamWriter[T](wbuff, 0, totalSize);
        val truncate = (x:Int) => 0;
        val readers = new Rail[ClockedReader[T]](readerCount.size,
            (idx:Int) => {
                val buff = new DoubleBuffer[Buffer[T]](clock, elem2, elem1);
                return new StreamReader[T](buff, offsets(idx), readerCount(idx), truncate);
        });
        return new Demux(writer, readers);
    }

    public static def main(args:Array[String](1)):void {
        finish async {
            val T = 3;
            val clock = Clock.make();
            val dem = make[Pair[Int,Int]](new Rail[Int](T, (x:Int) => x + 1), clock, Pair[Int,Int](0, 0));
            val N = 10;
            val writer = dem.writer;
            async clocked(clock) {
                for (p in 1..N) {
                    for (x in 1..6) {
                        writer() = Pair[Int,Int](p, x);
                    }
                }
                writer.close();
            }

            for (reader in dem.readers.values()) {
                async clocked(clock) {
                    try {
                        while (true) {
                            val result = reader();
                            Console.OUT.println(result);
                        }
                    } catch (EOSException) {
                    }
                }
            }
        }
    }

}
