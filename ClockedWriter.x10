public interface ClockedWriter[T] extends Writer[T] {
    property def clock(): Clock;
}

