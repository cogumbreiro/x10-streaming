public interface Writer[T] {
    operator this()=(value:T):void;
    def set(value:T):void;
    def close():void;
}

